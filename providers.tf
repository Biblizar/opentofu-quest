terraform {
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = ">= 0.50.0"
    }
  }
}

provider "proxmox" {
  endpoint  = var.pve_host_address
  api_token = var.pve_api_token
  username  = var.username
  password  = var.password
  insecure  = true
  ssh {
    agent    = true
    username = var.pve_api_user
  }
}