variable "ct_bridge" {
  type = string
}
variable "ct_datastore_storage_location" {
  type = string
}
variable "ct_datastore_location" {
  type = string
}
variable "ct_disk_size" {
  type    = string
  default = "20"
}
variable "ct_nic_rate_limit" {
  type = number
}
variable "ct_memory" {
  type = number
}
variable "ct_source_file_path" {
  type = string
}
variable "dns_domain" {
  type = string
}
variable "dns_server" {
  type = string
}
variable "gateway" {
  type = string
}
variable "os_type" {
  type = string
}
variable "pve_api_token" {
  type = string
}
variable "pve_api_user" {
  type = string
}
variable "pve_host_address" {
  type = string
}
variable "tmp_dir" {
  type = string
}
variable "addrs" {
  description = "The ip v4 you want to target"
  type        = string
}
variable "pubkey" {
  description = "The pubkey to log in your env"
  type        = list(string)
}

variable "username" {
  description = "Username used by proxmox provider"
  type        = string
}
variable "password" {
  description = "The proxmox password used for entering in the proxmox user opentofu"
}