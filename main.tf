### container.tf
# see https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password.html. It will download "hashicorp/random" provider
resource "random_password" "root_password" {
  length  = 24
  special = false
}

# location of containers templates
resource "proxmox_virtual_environment_file" "debian_template" {
  content_type = "vztmpl"
  datastore_id = var.ct_datastore_location
  node_name    = "pve1"

  source_file {
    path = var.ct_source_file_path
  }
}

resource "proxmox_virtual_environment_container" "debian_container" {
  description   = "Managed by OpenTofu"
  node_name     = "pve1"
  start_on_boot = true
  tags          = ["linux", "infra"]
  unprivileged  = true
  vm_id         = 241001

  cpu {
    architecture = "amd64"
    cores        = 1
  }

  disk {
    datastore_id = var.ct_datastore_storage_location
    size         = var.ct_disk_size
  }

  memory {
    dedicated = var.ct_memory
    swap      = 0
  }

  operating_system {
    template_file_id = proxmox_virtual_environment_file.debian_template.id
    type             = var.os_type
  }

  initialization {
    hostname = "ct-quest"

    dns {
      domain = var.dns_domain
      server = var.dns_server
    }

    ip_config {
      ipv4 {
        address = var.addrs
        gateway = var.gateway
      }
    }
    user_account {
      keys     = var.pubkey
      password = random_password.root_password.result
    }
  }
  network_interface {
    name       = var.ct_bridge
    rate_limit = var.ct_nic_rate_limit
  }

  features {
    nesting = true
    fuse    = false
  }
}